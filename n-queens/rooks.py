from pprint import pprint


def is_in_danger(board, row_index, column_index):
    for index,value in enumerate(board [row_index]):
        if value == 0 or index == column_index:
            pass
        else:
            return True
    return False

def place_rook_in_column(board, column_index):
    if column_index >= 4:
        return
    for row_number in range(4):
        board[row_number][column_index]=1
        if is_in_danger(board, row_number, column_index):
            board[row_number][column_index] = 0
        else:
            place_rook_in_column(board, column_index + 1)



def four_rooks():
    board = [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ]

    place_rook_in_column(board, 0)

    return board



if __name__ == "__main__":
    board = four_rooks()
    pprint(board, width=20)
