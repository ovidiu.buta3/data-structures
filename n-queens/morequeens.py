def get_chessboard():
    """
    Chessboard as a vector. 0-> empty chess cell, 1-> queen cell
    """
    chessboard = [1, 0, 0, 0, 0, 0, 0, 1,
                  0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0,
                  ]

    return chessboard


def check_queen_horizontally(chessboard, index):
    """ Check where queen is, from a to h """
    horizontal_position = index % 8
    first_elem_of_line = index - horizontal_position
    last_elem_of_line = index + 8 - horizontal_position

    for neighbour in range(first_elem_of_line, last_elem_of_line):
        if neighbour == index:  # We ignore this case, since this is our queen
            continue
        # print(neighbour, chessboard[neighbour])
        if chessboard[neighbour]:
            print("queens are not having fun horizontally!")
            return False
    return True


def check_queen_vertically(index, chessboard):
    """ Check where queen is vertically, from 1 to 8, then we iterate thorugh all other vertical values """
    vertical_position = int(index / 8)
    line_of_queen = index % 8
    for col in range(0, 8):
        if col == vertical_position:  # This should be our queen
            # print("this should be our queen, vertically ", vertical_position, index)
            continue
        neighbour = col * 8 + line_of_queen  # Every vertical value corresponding to queen's position
        # print(neighbour, col, chessboard[neighbour])
        if chessboard[neighbour]:
            print("queens are not having fun vertically!")
            return False
    return True


def check_queen_attack_on_diagonal(chessboard, index):
    """
    Check for every queen if they attack each other diagonal
    :param index: index of queen from chessboard list/vector
    :param chessboard: the vector of boolean values
    :return: True if queens are ok diagonally, else False
    """

    vertical_position = int(index / 8)
    horizontal_position = index % 8

    # left diagonal

    left_h = horizontal_position
    left_v = vertical_position

    while left_v < 7 and left_h > 1:
        left_h = left_h - 1
        left_v = left_v + 1
        print(left_v, left_h)
        if chessboard[left_v * 8 + left_h]:
            print("problems with left diagonal")
            return False

    # right diagonal

    right_h = horizontal_position
    right_v = vertical_position
    while right_v < 7 and right_h < 7:
        right_v = right_v + 1
        right_h = right_h + 1
        # print(right_v, right_h, horizontal_position, vertical_position, index)
        if chessboard[right_v * 8 + right_h]:
            print("problems with right diagonal")
            return False
    return True


def queen_attack_each_other(chessboard):
    """ function that validates the queens to not attack each other """
    valid_chessboard = True

    for index, cell in enumerate(chessboard):
        if cell:  # We have a queen on the board
            print("Queen spotted having coordonates: ", int(index / 8), index % 8)
            valid_chessboard = check_queen_horizontally(chessboard, index) and check_queen_vertically(index,
                                                                                                      chessboard) and check_queen_attack_on_diagonal(
                chessboard, index)
            if not valid_chessboard:
                return False

    return True


def queen_problem():
    chessboard = get_chessboard()
    is_valid = queen_attack_each_other(chessboard)
    if is_valid:
        print("The chessboard is valid! Those queens are friends!")
    else:
        print("The chessboard is in war! These queens are some bad bitches!")


if __name__ == '__main__':
    queen_problem()
